var Template = function(context){

	var chunk = [];

	if(!context.page.title) context.page.title = context.config.app.title;

	var head = [

		'<!DOCTYPE html>',
		'<html lang="en">',
		'<head>',
		'<meta charset="utf-8">',
		'<meta http-equiv="X-UA-Compatible" content="IE=edge">',
		'<meta name="viewport" content="width=device-width, initial-scale=1">',
		'<title>' + context.page.title + '</title>',
		'<meta name="description" content="' + context.config.app.description + '">',
		'<link rel="stylesheet" type="text/css" href="/style/screen.' + context.config.app.version + '.' + context.config.app.build + '.min.css">',
		'<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">',
		'</head>'
	];

	var header = [

		'<body>',
		'<div class="header">',
		'<h1><a href="/" title="' + context.config.app.name + '">' + context.config.app.name + '</a></h1>',
		'<h2>' + context.config.app.description + '</h2>',
		'</div>'
	        
	];

	chunk = head.concat(header);

	return chunk;

}
module.exports.Template = Template;
