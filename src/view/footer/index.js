var Template = function(context){

	var chunk = [

		'<div class="footer">',
		'<p>' + context.config.app.name + ' version ' + context.config.app.version + '</p>',
		'</div>',
		'<div id="link-top"><a href="#top" title="Go to top of page">&#x25B2;</a></div>',
		'<script type="text/javascript" src="/javascript/client.' + context.config.app.version + '.' + context.config.app.build + '.min.js"></script>',
		'</body></html>'
	
	];

	return chunk;

}
module.exports.Template = Template;