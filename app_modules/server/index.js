var http = require('http');

var Init = function(dir){

	http.createServer(function (request, response) {

		var context = {

			"request"  : request,
			"response" : response,
			"dir" : dir,
			"config" : require(dir + 'config.json'),
			"module" : {

				"common" : require(dir + 'app_modules/common/index'),
				"router" : require(dir + 'app_modules/router/index'),
				"view" : require(dir + 'app_modules/view/index')
			},

			"routes"   : {},
			"page" : {
				"id" : null,
				"title" : undefined,
			},
			"process" : {
				"mode" : null,
				"layout" : null,
				"part" : null,
				"accept_encoding" : null,
				"success" : 0
			},
			"timer" : {
				"start" : process.hrtime(),
				"result" : 0
			}

		};

		context.module.router.Serve(context);

	}).listen(8080);

}
module.exports.Init = Init;

