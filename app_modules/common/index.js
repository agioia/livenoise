var fs = require('fs');
var moment = require('moment');

var Error = function(error, scope, context){

	var datestamp = moment().format('MMMM Do YYYY, h:mm:ss a');

	if(!error) error = datestamp + ' - Application error. Unable to continue';

	fs.appendFile(context.dir + '/logs/error-node.log', datestamp + ' - ' + error.toString() + ' - ' + JSON.stringify(context.request.headers) + '\n', function (err) {

	        context.response.writeHead(302, {'content-type':'text/html', 'Location': context.config.app.url + '/' + scope + '.htm'});
	        context.response.end();

	});

}
module.exports.Error = Error;

var Timer = function(timer){

        return (process.hrtime(timer.start)[1]  / 1e9).toFixed(6);

}
module.exports.Timer = Timer;
