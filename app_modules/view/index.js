var async = require('async');
var highland = require('highland');
var zlib = require('zlib');

var Render = function(context){

	var header = require(context.dir + 'src/view/header/index');
	var footer = require(context.dir + 'src/view/footer/index');
	context.process.part = require(context.dir + 'src/view/' + context.page.id + '/index');

	if(context.process.mode == 'full'){

		context.process.layout = highland([

			highland(header.Template(context)),
			highland(context.process.part.Template(context)),
			highland(footer.Template(context)),
			highland.nil

		]).sequence();

	}

	if(context.process.mode == 'part'){

		context.process.layout = highland([

			highland(context.process.part.Template(context)),
			highland.nil

		]).sequence();

	}

	context.process.part = null;

	context.process.accept_encoding = context.request.headers['accept-encoding'] || '';

	if (context.process.accept_encoding.match(/\bdeflate\b/)) {

		context.response.writeHead(200, {'content-type':'text/html', 'content-encoding' : 'deflate'});
		context.process.layout.pipe(zlib.createDeflate()).pipe(context.response);

	} else if (context.process.accept_encoding.match(/\bgzip\b/)) {

		context.response.writeHead(200, {'content-type':'text/html', 'content-encoding' : 'gzip'});
		context.process.layout.pipe(zlib.createGzip()).pipe(context.response);

	} else {

		context.response.writeHead(200, {'content-type':'text/html'});
		layout.pipe(context.response);

	}

	console.log('> response [' + context.request.method + '] in ' + context.module.common.Timer(context.timer) + ' seconds');

}
module.exports.Render = Render;
