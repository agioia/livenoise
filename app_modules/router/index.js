var async = require('async');

var Serve = function(context){

	async.waterfall([

		function Router(callback){

			context.routes = {

				// ajax request
				'/play/' : function Play_Part(){

					context.page.id = "play";
					context.page.title = "Live streaming";
					context.process.mode = 'part';
					(context.request.method.toLowerCase() == 'get') ? context.module.view.Render(context) : context.module.common.Error(context.config.error.request, 'error-request', context);

				},

				// regular request
				'/play-full/' : function Play_Full(){

					context.page.id = "play";
					context.page.title = "Live streaming";
					context.process.mode = 'full';
					(context.request.method.toLowerCase() == 'get') ? context.module.view.Render(context) : context.module.common.Error(context.config.error.request, 'error-request', context);

				}

			}

			context.routes.hasOwnProperty(context.request.url) ? callback(null, null, context) : callback(context.config.error.notfound, 'error-notfound', context);

		}

	], function Dispach(error, errorcode, result){

		(!error) ? result.routes[result.request.url]() : result.module.common.Error(error,  'error-notfound', result);

	});

}
module.exports.Serve = Serve;
