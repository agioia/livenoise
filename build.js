var async = require('async');
var fs = require('fs-extra');
var compressor = require('node-minify');
var highland = require('highland');
var build = require('./src/config/build.json');

var Init = function(){

	console.log('\n# liveNoise Build CLI version ' + build.version);
	console.log('# You can configure program and tasks editing /src/config/build.json\n');

	process.stdin.setEncoding('utf8');
	var task = process.argv.slice(2)[0].toLowerCase();

	console.log('# Task selected [' + task + ']\n');

	var tasks = {

		'static' : function Execute_Static_Task(){
			Static();

		},

		'font' : function Execute_Font_Task(){

			Font();

		},

		'image' : function Execute_Static_Task(){

			Image();

		},

		'clean' : function Execute_Static_Task(){

			Clean();

		},

		'all' : function Execute_Static_Task(){

			All();

		}

	}

	tasks.hasOwnProperty(task) ? tasks[task]() : console.log('# Task not valid.');

}

var Static = function(cbx){

	console.log('# [static] Task ready');

	var fingerprint = (Number(Date.now()) / 1024).toFixed(0).slice(-5);
	console.log('# [static] Build fingerprint: ' + fingerprint);

	async.waterfall([

		function Config_Task(callback) {

			async.waterfall([

				function Config_Task_Collect(inner_callback) {

					console.log('# [static] [config] Files: ' + build.static.config.files);

					var list = Get_Filename(build.static.config.files, 'config', 'json');
					var i = 0;
					var arr = [];
					for(i; i < list.length; i++)
						arr.push(require(list[i]));

					var config = {};

			    	async.eachSeries(arr, function(obj, cb) {

			    		var k = Object.keys(obj);
			    		var i = 0;
			    		for(i; i < k.length; i++)
			    			config[k[i]] = obj[k[i]];

			    		cb(null);


					}, function(error){

						if(error) inner_callback(error);
					    if(!error) config.app.build = fingerprint;
					    inner_callback(null, config);

					});
				},

				function Config_Task_Write(config, inner_callback) {

					fs.writeFile(__dirname + build.root + 'config.json', JSON.stringify(config), function(error) {

					    (!error) ? Success('# [static] [config] New config.json saved in application root', inner_callback) : inner_callback(error);

					});

				}


			],function (error) {

				(!error) ? callback(null) : callback(error);

			});

		},

		function Public_Dir(callback){

			fs.ensureDir(__dirname + build.dest, function(error) {

				(!error) ? callback(null) : callback(error);

			});

		},

		function Style_Task(callback){

			var config = require('./config.json');

			console.log('# [static] [style] Files: ' + build.static.style.files);
			var list = Get_Filename(build.static.style.files, 'style', 'css');

			new compressor.minify({
				type: 'yui-css',
				fileIn: list,
				fileOut: __dirname + build.dest + 'style/screen.' + config.app.version + '.' + config.app.build + '.min.css',

					callback: function(error, result){

						(!error) ? Success('# [static] [style] New screen.' + config.app.version + '.' + config.app.build + '.min.css saved in /public/style/', callback) : callback(error);

					}

			});
		},

		function Javascript_Task(callback) {

			var config = require('./config.json');

			console.log('# [static] [javascript] Files: ' + build.static.javascript.files);
			var list = Get_Filename(build.static.javascript.files, 'javascript', 'js');

			new compressor.minify({
				type: 'yui-js',
				fileIn: list,
				fileOut: __dirname + build.dest + 'javascript/client.' + config.app.version + '.' + config.app.build + '.min.js',

					callback: function(error, result){

						(!error) ? Success('# [static] [javascript] New client.' + config.app.version + '.' + config.app.build + '.min.js saved in /public/javascript/', callback) : callback(error);

					}

			});

		},

		function Html_Task(callback) {

			var config = require('./config.json');

			console.log('# [static] [html] Files: ' + build.static.html.files);
			var list = build.static.html.files;

	    	async.eachSeries(list, function(scope, cb) {

				async.waterfall([

					function Set_Stream(inner_callback){

						var context = {

							"config" : config,
							"page" : {
								"id" : scope
							}
						}

						var header = require('./src/view/header/index');
						var footer = require('./src/view/footer/index');
						var part = require('./src/view/' + scope + '/index');

						if(scope == 'play'){

							var streams = highland([

								highland(part.Template(context)),
								highland.nil

							]);

						}else{

							var streams = highland([

								highland(header.Template(context)),
								highland(part.Template(context)),
								highland(footer.Template(context)),
								highland.nil

							]);

						}

						inner_callback(null, streams);

					},

					function Sequence_Streams(streams, inner_callback){

						var html = streams.sequence();

						inner_callback(null, html);

					},

					function Streams_Write(html, inner_callback){

						var output = fs.createWriteStream(__dirname + build.dest + scope + '.htm');
						html.pipe(output);
						console.log('# [static] [html] New ' + scope + '.htm saved in /public/');
						inner_callback(null);

					}


				],function (error) {

					(!error) ? cb(null) : cb(error);

				});


			}, function(error){

				(!error) ? callback(null, 'end') : callback(error);

			});
		}


	],function (error, results) {

		console.log('# [static] Task end. ');

		if((cbx) && (results == 'end')) cbx(null);

		((!error) && (results == 'end')) ? console.log('# Build (' + fingerprint + ') finished with success. ') : console.log('# Build not completed. Error(s): ' + error);

	});

}

var Font = function(cbx){

	console.log('# [font] Task ready');

	var css = [

		'@charset "utf-8";'
	];

	console.log('# [font] Fonts: ' + build.font);

	async.waterfall([

		function Fonts_Files(callback){

			async.eachSeries(build.font, function(font, cb) {

				console.log('# [font] Font: ' + font);

				async.waterfall([

					function Fonts_Collect(callback){

						fs.readdir(__dirname + build.src + 'font/' + font, function(error, files){

							(!error) ? callback(null, files) : callback(error);

						});

					},

					function Fonts_Dir(files, callback){

						fs.ensureDir(__dirname + build.dest + 'font/' + font, function(error) {

							(!error) ? callback(null, files) : callback(error);

						});

					},

					function Fonts_Copy(files, callback){

						async.eachSeries(files, function(file, inner_cb) {

							fs.copy(__dirname + build.src + 'font/' + font + '/' + file, __dirname + build.dest + 'font/' + font + '/' + file, function(error) {

								if(!error) {
									console.log('# [font] ' + font + ' - File ' + file + ' copied to /public/font/' + font + '/');
									inner_cb(null);
								}else{
									inner_cb(error);
								}

							});

						}, function(error){

							(!error) ? callback(null, files) : callback(error);

						});
					},

					function Fonts_Css(files, callback){

						font_css = Font_Css_Template(font, files);

						css.push(font_css);

						callback(null);

					}


				],function (error) {

					(!error) ? cb(null) : cb(error);

				});


			}, function (error){

				(!error) ? callback(null) : callback(error);


			});

		},

		function Fonts_Css_Write(callback){

			var once = Number(build.font.length) + 1;
			var i = 0;
			var arr = [];
			for(i; i < once; i++){

				arr.push(css[i]);
			}

			var flatten = [];
			flatten = flatten.concat.apply(flatten, arr);

			fs.outputFile(__dirname + build.src + 'style/fonts.css', flatten.join(' '), function(error) {

				if(!error) {
					console.log('# [font] [style] Updated fonts.css in /src/font/');
					callback(null);
				}else{
					callback(error);
				}

			});

		}


	],function (error) {

		if((cbx) && (!error)) cbx(null);

		(!error) ? console.log('# [font] Task end. ') : console.log(error);

	});

}

var Image = function(cbx){

	console.log('# [image] Task ready');

	async.waterfall([

		function Images_Collect(callback){

			fs.readdir(__dirname + build.src + 'image/', function(error, files){

				if(!error) callback(null, files);

			})

		},

		function Images_Copy(files, callback){

			if(files.length == 0) files = 0;
			console.log('# [image] Files: ' + files);

			async.eachSeries(files, function(file, inner_cb) {

				var dest = __dirname + build.dest + 'image/' + file;

				if(file == 'favicon.ico') dest = __dirname + build.dest + file;

				fs.copy(__dirname + build.src + 'image/' + file, dest, function(error) {

					if(!error) {
						(file == 'favicon.ico') ? console.log('# [image] File ' + file + ' copied to /public/') : console.log('# [image] File ' + file + ' copied to /public/image/');
						inner_cb(null);
					}else{
						inner_cb(error);
					}

				});

			}, function(error){

				(!error) ? callback(null) : callback(error);

			});

		}


	],function (error) {

		if((cbx) && (!error)) cbx(null);

		(!error) ? console.log('# [image] Task end. ') : console.log(error);

	});

}

var Clean = function(cbx){

	console.log('# [clean] Task ready');

	async.waterfall([

		function Files_Remove(callback){

			async.eachSeries(build.clean.files, function(file, inner_cb) {

				fs.remove(__dirname + '/' + file, function(error) {

					if(!error) {
						console.log('# [clean] File ' + file + ' removed');
						inner_cb(null);
					}else{
						inner_cb(error);
					}

				});

			}, function(error){

				(!error) ? callback(null) : callback(error);

			});

		},

		function Dir_Remove(callback){

			async.eachSeries(build.clean.dir, function(dir, inner_cb) {

				fs.remove(__dirname + '/' + dir, function(error) {

					if(!error) {
						console.log('# [clean] Directory ' + dir + ' removed');
						inner_cb(null);
					}else{
						inner_cb(error);
					}

				});

			}, function(error){

				(!error) ? callback(null) : callback(error);

			});

		},

		function Public_Dir(callback){

			fs.ensureDir(__dirname + build.dest, function(error) {

				(!error) ? callback(null) : callback(error);

			});

		}


	],function (error) {

		if((cbx) && (!error)) cbx(null);

		(!error) ? console.log('# [clean] Task end. ') : console.log(error);

	});

}

var All = function(){

	console.log('# [all] Task ready');

	console.log('# [all] Tasks: ' + build.all);

	async.waterfall([

		function Clean_Task(callback){

			Clean(function(error){

				if(!error) callback(null);

			});

		},

		function Image_Task(callback){

			Image(function(error){

				if(!error) callback(null);

			});

		},

		function Font_Task(callback){

			Font(function(error){

				if(!error) callback(null);

			});

		},

		function Static_Task(callback){

			Static(function(error){

				if(!error) callback(null);

			});

		}


	],function (error) {

		(!error) ? console.log('# [all] Task end. ') : console.log(error);

	});

}

var Success = function(msg, callback){

	console.log(msg);
	callback(null);
}

var Get_Filename = function(list, task, ext){

	var i = 0;
	var arr = [];
	for(i; i < list.length; i++)
		arr.push(__dirname + build.src + task + '/' + list[i] + '.' + ext);
	return arr;

}

var Font_Css_Template = function(font, files){

	var chunk = [
		'@font-face{',
	    'font-family: "' + font + '";',
	    'font-weight: "normal";',
	    'font-style: "normal";',
	    'font-stretch: "normal";',
	    'src:',
	    Font_Url(font, files),
	    '}',
	    'body{',
	    'font-family: \'' + font + '\', \'HelveticaNeue-Light\', \'Helvetica Neue Light\', \'Helvetica Neue\', Helvetica, Arial, \'Lucida Grande\', sans-serif;',
	    '}'
	];

	function Font_Url(font, files){

		var i = 0;
		var arr = [];
		for(i; i < files.length; i++){

			if(files[i].split('.')[1].toLowerCase() == 'eot') arr.push('url(\'../font/' + font + '/' + files[i] + '\') format(\'embedded-opentype\')');
			if(files[i].split('.')[1].toLowerCase() == 'otf') arr.push('url(\'../font/' + font + '/' + files[i] + '\') format(\'opentype\')');
			if(files[i].split('.')[1].toLowerCase() == 'ttf') arr.push('url(\'../font/' + font + '/' + files[i] + '\') format(\'truetype\')');
			if(files[i].split('.')[1].toLowerCase() == 'woff') arr.push('url(\'../font/' + font + '/' + files[i] + '\') format(\'woff\')');
			if(files[i].split('.')[1].toLowerCase() == 'svg') arr.push('url(\'../font/' + font + '/' + files[i] + '#' + font + '\') format(\'svg\')');
		}

		return arr.join(", ").toString();
	}

	return chunk;

}

Init();
